import logging
import os
import xml.etree.cElementTree as ET
from enum import Enum

from lib.mplatform import Platform


class SchedulabilityStatus(Enum):
    SCHEDULABLE = 1
    SCHEDULABLE_DEGRADED = 2
    NOT_SCHEDULABLE = 3


class SchedulabilityType(Enum):
    NOT_A_SCHEDULABILITY_ANALYSIS = 1
    CHEDDAR = 2


class SchedulabilityAnalysis:
    name: str

    def __init__(self, name: str = ""):
        self.name = name

    def is_schedulable(self, platform: Platform, name: str) -> SchedulabilityStatus:
        return SchedulabilityStatus.NOT_SCHEDULABLE


class NotASchedulabilityAnalysis(SchedulabilityAnalysis):
    def is_schedulable(self, platform: Platform, name: str) -> SchedulabilityStatus:
        return SchedulabilityStatus.SCHEDULABLE


class CheddarAnalysis(SchedulabilityAnalysis):
    def is_schedulable(self, platform, state_name="scenario") -> SchedulabilityStatus:
        logging.info(f"Running CHEDDAR analysis on {state_name}")
        self.export(platform, f"states_cheddar\\{state_name}")
        os.system(f"run_cheddar.bat states_cheddar\\{state_name}\\")

        with open(f"states_cheddar\\{state_name}\\result.txt") as f:
            content = f.read()
            if "missed its deadline" in content:
                logging.info(f"{state_name} NOT schedulable")
                return SchedulabilityStatus.NOT_SCHEDULABLE
            if "No deadline missed in the computed scheduling" in content:
                logging.info(f"{state_name} schedulable")
                return SchedulabilityStatus.SCHEDULABLE

        logging.info(f"{state_name} NOT schedulable")
        return SchedulabilityStatus.NOT_SCHEDULABLE

    def export(self, platform: Platform, file_location_path: str):
        try:
            os.mkdir(file_location_path)
        except FileExistsError:
            pass

        root = ET.Element("cheddar")
        core_units = ET.SubElement(root, "core_units")
        processors = ET.SubElement(root, "processors")
        address_spaces = ET.SubElement(root, "address_spaces")
        tasks = ET.SubElement(root, "tasks")
        c = []
        p = []
        a = []
        t = []
        index = 0
        current_id = 5
        p_index = 0
        t_index = 0
        for computer in platform.computers:
            c.append(ET.SubElement(core_units, "core_unit", id=str(index + 1)))
            ET.SubElement(c[index], "object_type").text = "Core_Object_Type"
            ET.SubElement(
                c[index], "name"
            ).text = "core_Pampaflie_system_impl_Instance.cpu_" + str(index + 1)
            scheduling = ET.SubElement(c[index], "scheduling")
            ET.SubElement(
                scheduling, "scheduler_type"
            ).text = "Hierarchical_Offline_Protocol"
            ET.SubElement(scheduling, "quantum").text = "0"
            ET.SubElement(scheduling, "preemptive_type").text = "Preemptive"
            ET.SubElement(scheduling, "capacity").text = "0"
            ET.SubElement(scheduling, "period").text = "0"
            ET.SubElement(scheduling, "priority").text = "0"
            ET.SubElement(
                scheduling, "user_defined_scheduler_source_file_name"
            ).text = f"pampaflie_partition_scheduling_C{index+1}.xml"
            ET.SubElement(scheduling, "start_time").text = "0"
            ET.SubElement(c[index], "speed").text = "1"

            p.append(
                ET.SubElement(processors, "mono_core_processor", id=str(current_id))
            )
            current_id += 1
            ET.SubElement(p[index], "object_type").text = "Processor_Object_Type"
            ET.SubElement(
                p[index], "name"
            ).text = "Pampaflie_system_impl_Instance.cpu_" + str(index + 1)
            ET.SubElement(p[index], "processor_type").text = "Monocore_type"
            ET.SubElement(p[index], "migration_type").text = "No_Migration_Type"
            ET.SubElement(p[index], "core", ref=str(index + 1))

            applications_per_partition = [
                len(partition.applications) for partition in computer.partitions
            ]
            partition_durations = [0] * len(computer.partitions)
            left_over_index = 0
            left_over = 0
            for i in reversed(range(len(computer.partitions))):
                if applications_per_partition[i] > 0:
                    partition_durations[i] = computer.partitions[i].size + left_over
                    left_over = 0
                    left_over_index = i
                else:
                    left_over += computer.partitions[i].size

            # none of the partitions in the computer has a application. A filler app is needed
            computer_with_no_application = all(
                size == 0 for size in partition_durations
            )

            partition_durations[left_over_index] += left_over

            partition_time = 0
            partition_event_table = ET.Element("event_table")
            event_index = 0
            for event_index, partition in enumerate(computer.partitions):
                # if computer_with_no_application:

                # elif len(partition.applications) > 0:
                #
                #
                if partition_durations[event_index] > 0:
                    ET.SubElement(partition_event_table, "time_unit").text = str(
                        int(partition_time)
                    )
                    time_unit_event = ET.SubElement(
                        partition_event_table, "time_unit_event"
                    )
                    ET.SubElement(
                        time_unit_event, "type_of_event"
                    ).text = "ADDRESS_SPACE_ACTIVATION"
                    ET.SubElement(
                        time_unit_event, "activation_address_space"
                    ).text = f"Pampaflie_system_impl_Instance.C{partition.address.computer}_{partition.address.partition}"
                    ET.SubElement(time_unit_event, "duration").text = str(
                        int(partition_durations[event_index] * 100)
                    )
                    partition_time += partition_durations[event_index] * 100

                    a.append(
                        ET.SubElement(
                            address_spaces, "address_space", id=str(current_id)
                        )
                    )
                    current_id += 1
                    ET.SubElement(
                        a[p_index], "object_type"
                    ).text = "Address_Space_Object_Type"
                    ET.SubElement(
                        a[p_index], "name"
                    ).text = f"Pampaflie_system_impl_Instance.C{partition.address.computer}_{partition.address.partition}"
                    ET.SubElement(
                        a[p_index], "cpu_name"
                    ).text = "Pampaflie_system_impl_Instance.cpu_" + str(
                        partition.address.computer
                    )
                    ET.SubElement(a[p_index], "text_memory_size").text = "0"
                    ET.SubElement(a[p_index], "stack_memory_size").text = "0"
                    ET.SubElement(a[p_index], "data_memory_size").text = "0"
                    ET.SubElement(a[p_index], "heap_memory_size").text = "0"

                    p_scheduling = ET.SubElement(a[p_index], "scheduling")
                    # ET.SubElement(p_scheduling, "scheduler_type").text = "POSIX_1003_HIGHEST_PRIORITY_FIRST_PROTOCOL"
                    ET.SubElement(
                        p_scheduling, "scheduler_type"
                    ).text = "rate_monotonic_protocol"
                    ET.SubElement(p_scheduling, "quantum").text = "0"
                    ET.SubElement(p_scheduling, "preemptive_type").text = "Preemptive"
                    ET.SubElement(p_scheduling, "capacity").text = "0"
                    ET.SubElement(p_scheduling, "period").text = "0"
                    ET.SubElement(p_scheduling, "priority").text = "0"
                    ET.SubElement(p_scheduling, "start_time").text = "0"

                    ET.SubElement(
                        a[p_index], "mils_confidentiality_level"
                    ).text = "UnClassified"
                    ET.SubElement(a[p_index], "mils_integrity_level").text = "Low"
                    ET.SubElement(a[p_index], "mils_component").text = "SLS"
                    ET.SubElement(a[p_index], "mils_partition").text = "Device"
                    ET.SubElement(a[p_index], "mils_compliant").text = "false"

                    for application in partition.applications:
                        t.append(
                            ET.SubElement(tasks, "periodic_task", id=str(current_id))
                        )
                        current_id += 1
                        ET.SubElement(
                            t[t_index], "object_type"
                        ).text = "Task_Object_Type"
                        ET.SubElement(
                            t[t_index], "name"
                        ).text = f"Pampaflie_system_impl_Instance.C{application.address.computer}_{application.address.partition}.task_{application.name}"
                        ET.SubElement(t[t_index], "task_type").text = "Periodic_Type"
                        ET.SubElement(
                            t[t_index], "cpu_name"
                        ).text = "Pampaflie_system_impl_Instance.cpu_" + str(
                            application.address.computer
                        )
                        ET.SubElement(
                            t[t_index], "address_space_name"
                        ).text = f"Pampaflie_system_impl_Instance.C{application.address.computer}_{application.address.partition}"

                        ET.SubElement(t[t_index], "capacity").text = str(
                            int(application.wcet * 100)
                        )
                        ET.SubElement(t[t_index], "deadline").text = str(
                            int(application.period * 100)
                        )
                        ET.SubElement(t[t_index], "start_time").text = "0"
                        ET.SubElement(t[t_index], "priority").text = str(
                            6 - application.dal
                        )
                        ET.SubElement(t[t_index], "blocking_time").text = "0"
                        ET.SubElement(t[t_index], "policy").text = "Sched_Fifo"
                        ET.SubElement(t[t_index], "offsets")
                        ET.SubElement(t[t_index], "text_memory_size").text = "0"
                        ET.SubElement(
                            t[t_index], "text_memory_start_address"
                        ).text = "0"
                        ET.SubElement(t[t_index], "stack_memory_size").text = "0"
                        ET.SubElement(t[t_index], "criticality").text = "0"
                        ET.SubElement(t[t_index], "context_switch_overhead").text = "0"
                        ET.SubElement(t[t_index], "cfg_relocatable").text = "false"
                        ET.SubElement(
                            t[t_index], "mils_confidentiality_level"
                        ).text = "UnClassified"
                        ET.SubElement(t[t_index], "mils_integrity_level").text = "Low"
                        ET.SubElement(t[t_index], "mils_component").text = "SLS"
                        ET.SubElement(t[t_index], "mils_task").text = "Application"
                        ET.SubElement(t[t_index], "mils_compliant").text = "false"
                        ET.SubElement(t[t_index], "access_memory_number").text = "0"
                        ET.SubElement(
                            t[t_index], "maximum_number_of_memory_request_per_job"
                        ).text = "0"
                        ET.SubElement(t[t_index], "period").text = str(
                            int(application.period * 100)
                        )
                        ET.SubElement(t[t_index], "jitter").text = "0"
                        ET.SubElement(t[t_index], "every").text = "0"

                        t_index += 1

                    p_index += 1

            if computer_with_no_application:
                t.append(ET.SubElement(tasks, "periodic_task", id=str(current_id)))
                current_id += 1
                t2_index = len(t) - 1
                ET.SubElement(t[t2_index], "object_type").text = "Task_Object_Type"
                ET.SubElement(
                    t[t2_index], "name"
                ).text = f"Pampaflie_system_impl_Instance.C{computer.computer_address}_1.task_filler"
                ET.SubElement(t[t2_index], "task_type").text = "Periodic_Type"
                ET.SubElement(
                    t[t2_index], "cpu_name"
                ).text = "Pampaflie_system_impl_Instance.cpu_" + str(
                    computer.computer_address
                )
                ET.SubElement(
                    t[t2_index], "address_space_name"
                ).text = (
                    f"Pampaflie_system_impl_Instance.C{computer.computer_address}_1"
                )

                ET.SubElement(t[t2_index], "capacity").text = str(int(1))
                ET.SubElement(t[t2_index], "deadline").text = str(int(800 * 100))
                ET.SubElement(t[t2_index], "start_time").text = "0"
                ET.SubElement(t[t2_index], "priority").text = str(1)
                ET.SubElement(t[t2_index], "blocking_time").text = "0"
                ET.SubElement(t[t2_index], "policy").text = "Sched_Fifo"
                ET.SubElement(t[t2_index], "offsets")
                ET.SubElement(t[t2_index], "text_memory_size").text = "0"
                ET.SubElement(t[t2_index], "text_memory_start_address").text = "0"
                ET.SubElement(t[t2_index], "stack_memory_size").text = "0"
                ET.SubElement(t[t2_index], "criticality").text = "0"
                ET.SubElement(t[t2_index], "context_switch_overhead").text = "0"
                ET.SubElement(t[t2_index], "cfg_relocatable").text = "false"
                ET.SubElement(
                    t[t2_index], "mils_confidentiality_level"
                ).text = "UnClassified"
                ET.SubElement(t[t2_index], "mils_integrity_level").text = "Low"
                ET.SubElement(t[t2_index], "mils_component").text = "SLS"
                ET.SubElement(t[t2_index], "mils_task").text = "Application"
                ET.SubElement(t[t2_index], "mils_compliant").text = "false"
                ET.SubElement(t[t2_index], "access_memory_number").text = "0"
                ET.SubElement(
                    t[t2_index], "maximum_number_of_memory_request_per_job"
                ).text = "0"
                ET.SubElement(t[t2_index], "period").text = str(int(800 * 100))
                ET.SubElement(t[t2_index], "jitter").text = "0"
                ET.SubElement(t[t2_index], "every").text = "0"

            partition_tree = ET.ElementTree(partition_event_table)
            ET.indent(partition_tree, space="\t", level=0)

            partition_tree.write(
                f"{file_location_path}\\pampaflie_partition_scheduling_C{index+1}.xml",
                xml_declaration=True,
                encoding="utf-8",
            )
            index += 1

        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)

        tree.write(
            f"{file_location_path}\\Pampaflie_system_impl_Instance.xmlv3",
            xml_declaration=True,
            encoding="utf-8",
        )
