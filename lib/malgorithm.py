import csv
import logging
from enum import Enum
from typing import List

import ahpy

import lib.mexceptions as mexceptions
from lib.mplatform import (
    Application,
    ApplicationAddress,
    ApplicationDAL,
    Computer,
    Partition,
    Platform,
)


class AlgorithmType(Enum):
    GREEDY = 1
    MINMIN = 2
    AHP = 3
    AHP_ROUNDROBIN = 4
    AHP_RESOURCE_AWARE = 5
    AHP_POST_BALANCE = 6
    MINMIN_NO_BALANCE = 7


class PlatformJournal:
    _journal: List[dict]

    def __init__(self):
        self._journal = []

    def record_allocated_application(self, application: Application):
        self._journal.append(
            {"type": "add", "address": application.address, "application": application}
        )

    def record_removed_application(self, application: Application):
        self._journal.append(
            {
                "type": "remove",
                "address": application.address,
                "application": application,
            }
        )

    def pop_journal_entry(self, index=-1):
        self._journal.pop(index)

    def __str__(self):
        output_str = ""
        for item in self._journal:
            output_str += f'{item["type"]} {item["application"].name} in C{item["address"].computer}P{item["address"].partition}\n'
        return output_str
        return str(self._journal)


class AllocationAlgorithm:
    journal: PlatformJournal
    name: str

    def __init__(self, name: str = ""):
        self._journal = PlatformJournal()
        if name == "":
            self.name = "Algorithm"
        else:
            self.name = name

    def allocate(self, applications: list[Application], platform: Platform):
        return applications

    def _pre_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        pass

    def _post_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        pass

    def _pre_allocate(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        pass

    def _pre_allocate_retry(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self._pre_allocate(applications, platform)

    def sort_applications_by_dal(
        self, applications: List[Application]
    ) -> List[Application]:
        """Buble sort the applications by ahp rating

        Args:
            partitions (List[Application]): list of applications to be sorted

        Returns:
            List[Application]: sorted application list
        """
        n = len(applications)

        for i in range(n):
            # Create a flag that will allow the function to
            # terminate early if there's nothing left to sort
            already_sorted = True

            # Start looking at each item of the list one by one,
            # comparing it with its adjacent value. With each
            # iteration, the portion of the array that you look at
            # shrinks because the remaining items have already been
            # sorted.
            for j in range(n - i - 1):
                if applications[j].dal > applications[j + 1].dal:
                    # If the item you're looking at is greater than its
                    # adjacent value, then swap them
                    applications[j], applications[j + 1] = (
                        applications[j + 1],
                        applications[j],
                    )

                    # Since you had to swap two elements,
                    # set the `already_sorted` flag to `False` so the
                    # algorithm doesn't finish prematurely
                    already_sorted = False

            # If there were no swaps during the last iteration,
            # the array is already sorted, and you can terminate
            if already_sorted:
                break

        return applications

    def balance(self, platform: Platform):

        logging.info("Balancing the platform")
        # get all the partition from the platform to get a sorted list of partition. It
        # facilitates the algorithm.
        partitions = []
        for computer in platform.computers:
            for partition in computer.partitions:
                partitions.append(partition)

        initial_load_ratio = platform.balance_ratio()
        current_load_ratio = initial_load_ratio
        previous_load_ratio = 0.0

        # self.print_partitions(partitions)

        # sorting parition so the ones with more load will be checked first
        partitions = self.sort_partitions_by_load(partitions)
        # logging.debug()
        # self.print_partitions(partitions)

        # Try to move all applications (one at a time)
        while not self.all_applications_moved(partitions):
            for partition in partitions:
                # TODO sort the application to the smaller one
                for application in partition.applications:
                    # try to reallocate only the application that weren't tried before
                    if not application.balance_try:
                        previous_load_ratio = current_load_ratio
                        moving_application = platform.pop_application_at_address(
                            application.address
                        )
                        self._journal.record_removed_application(moving_application)
                        # TODO run through the partition on the inverse order

                        best_allocated_address = moving_application.address
                        # Try in all other partitions to check if balance ratio is improved
                        for partition in partitions:
                            computer_address = partition.address.computer - 1

                            # check if application is allowed to be allocated in this comptuer
                            try:
                                platform.computers[computer_address].check_constraints(
                                    moving_application
                                )
                            except mexceptions.ApplicationDependendConstraint:
                                # moving_application.balance_try = True
                                continue

                            try:
                                partition.append_application(moving_application)
                                new_load_ratio = platform.balance_ratio()

                                # if the balance ratio didn't improve, retry in another partition
                                if new_load_ratio < current_load_ratio:
                                    best_allocated_address = moving_application.address
                                    current_load_ratio = new_load_ratio

                                moving_application = partition.pop_application()
                                # else:
                                #     current_load_ratio = new_load_ratio
                                #     self._journal.record_allocated_application(moving_application)
                                #     continue

                                # moving_application.balance_try = True
                                # break

                            except (
                                mexceptions.PartitionFullError,
                                mexceptions.PartitionCritical,
                            ) as e:
                                # moving_application.balance_try = True
                                continue
                        platform.append_application_at_address(
                            best_allocated_address, moving_application
                        )
                        self._journal.record_allocated_application(moving_application)
                        moving_application.balance_try = True

                        # partition loads were changed, needs to be resorted
                        if current_load_ratio < previous_load_ratio:
                            partitions = self.sort_partitions_by_load(partitions)
                            # self.print_partitions(partitions)
                            break
                # partition loads were changed, needs to be resorted
                if current_load_ratio < previous_load_ratio:
                    previous_load_ratio = current_load_ratio
                    break

    def run(
        self,
        applications: list[Application],
        platform: Platform,
        fail_analysis: bool = False,
    ):
        logging.debug(
            f"Allocating {len(applications)} applications into Platform ({len(platform.computers)})"
        )

        self._pre_run(applications, platform)

        self._pre_allocate(applications, platform)
        not_allocated_applications = self.allocate(applications, platform)

        allocation_failed = False
        for application in not_allocated_applications:
            if application.dal <= ApplicationDAL.B:
                allocation_failed = True
                break

        if allocation_failed:
            # try to remove DAL D applications first
            remove_application_level = [ApplicationDAL.D, ApplicationDAL.C]
            lower_criticality_applications = []
            for computer in platform.computers:
                for partition in computer.partitions:
                    for application in partition.applications:
                        if application.dal in remove_application_level:
                            lower_criticality_applications.append(
                                platform.pop_application_at_address(application.address)
                            )

            not_allocated_applications.extend(lower_criticality_applications)
            self._pre_allocate_retry(not_allocated_applications, platform)
            not_allocated_applications_2 = self.allocate(
                not_allocated_applications, platform
            )
            for application in not_allocated_applications_2:
                if application.dal <= ApplicationDAL.B:
                    raise mexceptions.AllocationError("No partition available")

            not_allocated_applications = not_allocated_applications_2

            # TODO define what apps to balance

        self._post_run(not_allocated_applications, platform)

        logging.debug(f"Finnished allocating {len(applications)} applications")

        return not_allocated_applications

    def print_partitions(self, partitions: List[Partition]):
        index = 0
        for partition in partitions:
            index += 1
            logging.debug(
                f"{index}: C{partition.address.computer}P{partition.address.partition} L: {partition.load()}"
            )

    def sort_partitions_by_load(
        self, partitions: List[Partition], ascendent=False
    ) -> List[Partition]:
        """Buble sort the partitions by load

        Args:
            partitions (List[Partition]): list of partitions to be sorted

        Returns:
            List[Partition]: sorted partition list
        """
        n = len(partitions)

        for i in range(n):
            # Create a flag that will allow the function to
            # terminate early if there's nothing left to sort
            already_sorted = True

            # Start looking at each item of the list one by one,
            # comparing it with its adjacent value. With each
            # iteration, the portion of the array that you look at
            # shrinks because the remaining items have already been
            # sorted.
            for j in range(n - i - 1):
                if ascendent:
                    condition = partitions[j].load() < partitions[j + 1].load()
                else:
                    condition = partitions[j].load() > partitions[j + 1].load()
                if condition:
                    # If the item you're looking at is greater than its
                    # adjacent value, then swap them
                    partitions[j], partitions[j + 1] = partitions[j + 1], partitions[j]

                    # Since you had to swap two elements,
                    # set the `already_sorted` flag to `False` so the
                    # algorithm doesn't finish prematurely
                    already_sorted = False

            # If there were no swaps during the last iteration,
            # the array is already sorted, and you can terminate
            if already_sorted:
                break

        return partitions

    def all_applications_moved(self, partitions: List[Partition]):
        for partition in partitions:
            for application in partition.applications:
                if not application.balance_try:
                    return False

        return True


class AlgorithmMinMin(AllocationAlgorithm):
    def __init__(self):
        super().__init__(name="minmin")

    def _post_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self.balance(platform)

    def _pre_allocate(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        # initialize applications - all are set to be moved if possible
        applications.sort()

    def allocate(self, applications: list[Application], platform: Platform = None):
        # allocate only removed applications
        not_allocated_applications = []
        for application in applications:
            application.balance_try = False
            # failed_allocation = False
            while applications:
                application = applications.pop(0)
                try:
                    self._internal_allocate(application, platform)
                except mexceptions.AllocationError as e:
                    not_allocated_applications.append(application)
                    logging.debug(
                        f"Application {application.name} could not be allocated"
                    )
                    if application.dal <= ApplicationDAL.B:
                        not_allocated_applications.extend(applications)
                        return not_allocated_applications
                        # failed_allocation = True
                        # break
                        raise mexceptions.AllocationError(e)

        return not_allocated_applications

    def _internal_allocate(self, application: Application, platform: Platform = None):
        if platform is None:
            platform = self.platform
        computer_index = 0
        partition_index = 0
        for computer in platform.computers:
            computer_index += 1
            for partition in computer.partitions:
                partition_index += 1
                try:
                    platform.append_application_at_address(
                        ApplicationAddress(partition_index, computer_index), application
                    )

                    self._journal.record_allocated_application(application)
                    return
                except mexceptions.ApplicationDependendConstraint as e:
                    logging.debug(f"{e}")
                    break
                except (
                    mexceptions.PartitionFullError,
                    mexceptions.PartitionCritical,
                ) as e:
                    logging.debug(f"{partition_index}: {e}")
                    continue
            partition_index = 0
            logging.debug(f"C{computer_index} no suitable for {application.name}")

        raise mexceptions.AllocationError(f"Could not allocate {application.name}")


class AlgorithmMinMinNoBalance(AlgorithmMinMin):
    def __init__(self):
        super().__init__()
        self.name = "minmin_no_balance"

    def _post_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        return


class AlgorithmGreedy(AllocationAlgorithm):
    def __init__(self):
        super().__init__(name="greedy")

    def allocate(self, applications: list[Application], platform: Platform):
        not_allocated_partitions = []

        while applications:
            allocated = False
            application = applications.pop()
            computer_index = 0
            partition_index = 0
            for computer in platform.computers:
                computer_index += 1
                for partition in computer.partitions:
                    partition_index += 1
                    try:
                        logging.debug(
                            f"Trying to allocation {application.name} to C{computer_index}P{partition_index}... "
                        )
                        platform.append_application_at_address(
                            ApplicationAddress(partition_index, computer_index),
                            application,
                        )
                        logging.debug(
                            f"C{computer_index}P{partition_index} now contains {application.name}"
                        )

                        self._journal.record_allocated_application(application)
                        allocated = True
                        break
                    except mexceptions.ApplicationDependendConstraint as e:
                        logging.debug(f"{e}")
                        break
                    except (
                        mexceptions.PartitionFullError,
                        mexceptions.PartitionCritical,
                    ) as e:
                        logging.debug(f"{partition_index}: {e}")
                        continue
                partition_index = 0
                logging.debug(f"C{computer_index} no suitable for {application.name}")
                if allocated:
                    break
            else:
                if not allocated:
                    not_allocated_partitions.append(application)

        return not_allocated_partitions

    def all_applications_moved(self, partitions: List[Partition]):
        for partition in partitions:
            for application in partition.applications:
                if not application.balance_try:
                    return False

        return True


class AlgorithmAHP(AllocationAlgorithm):
    _preferences_matrix: list[list] = None
    _app_ratings: ahpy.Compare = None

    def __init__(self, preferences_file_path: str = None) -> None:
        super().__init__(name="ahp")
        self._preferences_matrix = []
        self._load_preferences(preferences_file_path)
        self._calculate_ratings()
        # print(self._preferences_matrix)
        logging.debug(self._preferences_matrix)
        logging.debug(self._app_ratings.target_weights)

    def _calculate_ratings(self):
        ahp_input = {}
        for row_index in range(1, len(self._preferences_matrix)):
            for app_index in range(1, len(self._preferences_matrix[row_index])):
                key = (
                    self._preferences_matrix[row_index][0],
                    self._preferences_matrix[0][app_index],
                )
                ahp_input[key] = self._preferences_matrix[row_index][app_index]

        self._app_ratings = ahpy.Compare(
            name="app_ratings", comparisons=ahp_input, precision=3, random_index="dd"
        )
        # print()
        # print(app_ratings.target_weights)
        # print(app_ratings.consistency_ratio)

    def _load_preferences(self, preferences_file_path):
        with open(
            preferences_file_path, newline="", encoding="utf-8-sig"
        ) as preferences_file:
            rating_data = csv.reader(preferences_file, delimiter=",")

            for row_index, row in enumerate(rating_data):
                preferences_list = []
                for item_index, item in enumerate(row):
                    new_item = row[item_index]
                    if item_index > 0 and row_index > 0:
                        new_item = float(new_item)
                    preferences_list.append(new_item)

                self._preferences_matrix.append(preferences_list)

    def sort_applications_by_ahp_ratings(
        self, applications: List[Application], ahp_ratings=None
    ) -> List[Application]:
        """Buble sort the applications by ahp rating

        Args:
            partitions (List[Application]): list of applications to be sorted

        Returns:
            List[Application]: sorted application list
        """
        n = len(applications)

        if ahp_ratings is None:
            ahp_ratings = self._app_ratings.target_weights

        for i in range(n):
            # Create a flag that will allow the function to
            # terminate early if there's nothing left to sort
            already_sorted = True

            # Start looking at each item of the list one by one,
            # comparing it with its adjacent value. With each
            # iteration, the portion of the array that you look at
            # shrinks because the remaining items have already been
            # sorted.
            for j in range(n - i - 1):
                if (
                    ahp_ratings[applications[j].name]
                    < ahp_ratings[applications[j + 1].name]
                ):
                    # If the item you're looking at is greater than its
                    # adjacent value, then swap them
                    applications[j], applications[j + 1] = (
                        applications[j + 1],
                        applications[j],
                    )

                    # Since you had to swap two elements,
                    # set the `already_sorted` flag to `False` so the
                    # algorithm doesn't finish prematurely
                    already_sorted = False

            # If there were no swaps during the last iteration,
            # the array is already sorted, and you can terminate
            if already_sorted:
                break

        return applications

    def _pre_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self.sort_applications_by_ahp_ratings(applications)

    def _post_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        pass

    def _pre_allocate(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self._partitions = []
        for computer in platform.computers:
            for partition in computer.partitions:
                self._partitions.append(partition)

    def _pre_allocate_retry(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self.sort_applications_by_ahp_ratings(applications)
        self.sort_applications_by_dal(applications)

    def allocate(self, applications: list[Application], platform: Platform):

        not_allocated_applications = []
        while applications:
            application = applications.pop(0)
            allocated = False

            for partition in self._partitions:
                try:
                    logging.debug(
                        f"Trying to allocation {application.name} to C{partition.address.computer}P{partition.address.partition}... "
                    )
                    platform.append_application_at_address(
                        partition.address, application
                    )
                    logging.debug(
                        f"C{partition.address.computer}P{partition.address.partition} now contains {application.name}"
                    )

                    self._journal.record_allocated_application(application)
                    allocated = True
                    break
                except mexceptions.ApplicationDependendConstraint as e:
                    logging.debug(f"{e}")
                    continue
                except (
                    mexceptions.PartitionFullError,
                    mexceptions.PartitionCritical,
                ) as e:
                    logging.debug(f"{partition.address.partition}: {e}")
                    continue

            if not allocated:
                not_allocated_applications.append(application)
                logging.debug(f"Application {application.name} could not be allocated")
                if application.dal <= ApplicationDAL.B:
                    not_allocated_applications.extend(applications)
                    return not_allocated_applications

        return not_allocated_applications


class AlgorithmAHPRoundRobin(AlgorithmAHP):
    def __init__(self, preferences_file_path: str = None) -> None:
        super().__init__(preferences_file_path)
        self.name = "ahp_roundrobin"

    def allocate(self, applications: list[Application], platform: Platform):

        not_allocated_applications = []
        partition_index = 0
        while applications:
            application = applications.pop(0)
            allocated = False

            for i in range(
                partition_index, partition_index + len(self._partitions) - 1
            ):
                # for partition in self._partitions:
                index = i % len(self._partitions)
                partition = self._partitions[index]
                partition_index += 1

                try:
                    logging.debug(
                        f"Trying to allocation {application.name} to C{partition.address.computer}P{partition.address.partition}... "
                    )
                    platform.append_application_at_address(
                        partition.address, application
                    )
                    logging.debug(
                        f"C{partition.address.computer}P{partition.address.partition} now contains {application.name}"
                    )

                    self._journal.record_allocated_application(application)
                    allocated = True
                    break
                except mexceptions.ApplicationDependendConstraint as e:
                    logging.debug(f"{e}")
                    continue
                except (
                    mexceptions.PartitionFullError,
                    mexceptions.PartitionCritical,
                ) as e:
                    logging.debug(f"{partition.address.partition}: {e}")
                    continue

            if not allocated:
                not_allocated_applications.append(application)
                logging.debug(f"Application {application.name} could not be allocated")
                if application.dal <= ApplicationDAL.B:
                    not_allocated_applications.extend(applications)
                    return not_allocated_applications

        return not_allocated_applications


class AlgorithmAHPPostBalance(AlgorithmAHP):
    def __init__(self, preferences_file_path: str = None) -> None:
        super().__init__(preferences_file_path)
        self.name = "ahp_post_balance"

    def _post_run(
        self,
        applications: list[Application],
        platform: Platform,
    ):
        self.balance(platform)


class AlgorithmAHPResourceAware(AlgorithmAHP):
    def __init__(self, preferences_file_path: str = None) -> None:
        super().__init__(preferences_file_path)
        self.name = "ahp_resource_aware"

    def allocate(self, applications: list[Application], platform: Platform):

        not_allocated_applications = []
        while applications:
            application = applications.pop(0)
            allocated = False
            self.sort_partitions_by_load(self._partitions, ascendent=True)
            for partition in self._partitions:

                try:
                    logging.debug(
                        f"Trying to allocation {application.name} to C{partition.address.computer}P{partition.address.partition}... "
                    )
                    platform.append_application_at_address(
                        partition.address, application
                    )
                    logging.debug(
                        f"C{partition.address.computer}P{partition.address.partition} now contains {application.name}"
                    )

                    self._journal.record_allocated_application(application)
                    allocated = True
                    break
                except mexceptions.ApplicationDependendConstraint as e:
                    logging.debug(f"{e}")
                    continue
                except (
                    mexceptions.PartitionFullError,
                    mexceptions.PartitionCritical,
                ) as e:
                    logging.debug(f"{partition.address.partition}: {e}")
                    continue

            if not allocated:
                not_allocated_applications.append(application)
                logging.debug(f"Application {application.name} could not be allocated")
                if application.dal <= ApplicationDAL.B:
                    not_allocated_applications.extend(applications)
                    return not_allocated_applications

        return not_allocated_applications
