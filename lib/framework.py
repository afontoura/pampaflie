import copy
import csv
import getopt
import json
import logging
import os
import sys
from datetime import datetime

# import lib.mexceptions
import lib.mexceptions as mexceptions
from lib.malgorithm import AllocationAlgorithm
from lib.manalysis import SchedulabilityAnalysis, SchedulabilityStatus
from lib.mplatform import Application, ApplicationDAL, Platform
from lib.msystemstates import SystemReconfigurationGraph, SystemState


def read_application(filename: str) -> list[Application]:
    csv_application = open(filename, newline="", encoding="utf-8-sig")
    application_data = csv.DictReader(csv_application)
    app_list = []
    for row in application_data:
        app_list.append(
            Application(
                name=row["Name"],
                period=float(row["Period"]),
                wcet=float(row["WCET"]),
                dal=ApplicationDAL(int(row["DAL"])),
                independent=row["Independent"],
            )
        )

    return app_list


def num_of_applications_in_platform(platform: Platform):
    count = 0
    for computer in platform.computers:
        for partition in computer.partitions:
            count += len(partition.applications)
    return count


def print_applications_in_platform(platform: Platform):
    count = 0
    for computer in platform.computers:
        for partition in computer.partitions:
            for application in partition.applications:
                logging.info(application.name)


def evaluate_system_status(
    m_system_state_status: SchedulabilityStatus,
    not_allocated_applications: list[Application],
) -> SchedulabilityStatus:
    if m_system_state_status == SchedulabilityStatus.NOT_SCHEDULABLE:
        return m_system_state_status
    elif len(not_allocated_applications) > 0:
        return SchedulabilityStatus.SCHEDULABLE_DEGRADED
    else:
        return SchedulabilityStatus.SCHEDULABLE


# for computer_index in range(len(platform.computers)):
def evaluate_computer_fault(
    computer_index: int,
    state: SystemState,
    algorithm: AllocationAlgorithm,
    analysis: SchedulabilityAnalysis,
    applications: list[Application] = None,
):
    # fail analysis
    logging.debug(f"Trying: {state.name} failing {computer_index}")

    # From paper: Ss = St (S0)
    test_platform = copy.deepcopy(state.platform)

    removed_computer_name = f"{test_platform.computers[computer_index].name}"
    # From paper: and Ci failed
    removed_applications = test_platform.remove_computer(computer_index)

    if applications is not None:
        removed_applications.extend(applications)

    not_allocated_applications = None
    try:
        logging.debug(
            f"Number of applications in the platform: {num_of_applications_in_platform(test_platform)}"
        )

        # From paper: for each SwItem_p not yet mapped
        not_allocated_applications = algorithm.run(
            removed_applications, test_platform, fail_analysis=True
        )
        logging.debug(
            f"Number of applications in the platform: {num_of_applications_in_platform(test_platform)}"
        )

        m_system_state_status = analysis.is_schedulable(
            test_platform, f"{algorithm.name}-{state.name}_{computer_index}"
        )
        system_state_status = evaluate_system_status(
            m_system_state_status, not_allocated_applications
        )
    except mexceptions.AllocationError as e:
        system_state_status = SchedulabilityStatus.NOT_SCHEDULABLE
        # logging.info(f'State {state.name}-{computer_index} not schedulable')

    new_state = SystemState(test_platform, f"{state.name}-{computer_index}")
    new_state.set_status(system_state_status)
    state.add_edge(new_state, removed_computer_name)

    logging.info(f"State {new_state.name}|{new_state.status} - C{computer_index}")
    logging.debug(test_platform)

    if system_state_status != SchedulabilityStatus.NOT_SCHEDULABLE:
        for computer in test_platform.computers:
            evaluate_computer_fault(
                computer.computer_address - 1,
                new_state,
                algorithm,
                analysis,
                not_allocated_applications,
            )

    return

    for application in removed_applications:
        test_platform.allocate(application, test_platform)

    # generate xml
    # evaluate
    return test_platform


def find_application(applications: list[Application], app: Application) -> int:
    for i, application in enumerate(applications):
        if app.name == application.name:
            return i
    else:
        return -1


def search_function_in_computers(
    platform: Platform, system_function: str, applications: list[Application]
) -> list:
    computer_list = []
    local_applications = copy.deepcopy(applications)
    for computer in platform.computers:
        for partition in computer.partitions:
            for application in partition.applications:
                index = find_application(local_applications, application)
                if index > -1:
                    local_applications.pop(index)
                    if computer.name not in computer_list:
                        computer_list.append(computer.name)

    if len(local_applications) == 0:
        return computer_list
    else:
        return []


def find_computer_index(platform: Platform, computer_name: str) -> int:
    for index, computer in enumerate(platform.computers):
        if computer.name == computer_name:
            return index
    else:
        return -1


def search_failure_paths(
    node: SystemState, system_function: str, applications: list[Application]
) -> list:
    participant_computers = search_function_in_computers(
        node.platform, system_function, applications
    )
    if (
        len(participant_computers) == 0
        # or node.status == SchedulabilityStatus.NOT_SCHEDULABLE
        or len(node.next_states) == 0
    ):
        return []

    path = []
    for computer in participant_computers:
        # index = find_computer_index(node.platform, computer)
        index = node.computer_failed_names.index(computer)
        leaf_path = search_failure_paths(
            node.next_states[index], system_function, applications
        )

        if len(leaf_path) > 0:
            for entry_path in leaf_path:
                # I'm not sure if a copy is necessary
                new_path = [computer]
                if type(entry_path) is str:
                    new_path.append(computer)
                else:
                    new_path.extend(entry_path)

                path.append(new_path)
        else:
            new_path = [computer]
            path.append(new_path)
    return path


def computer_failure_rate(platform: Platform, computer_name: str) -> float:
    return platform.computers[find_computer_index(platform, computer_name)].failure_rate


def calculate_function_failure_rate(platform: Platform, paths: list):
    function_failure_rate = 0
    count_equal = 1
    for path in paths:
        path_failure_rate = 1
        for computer_name in path:
            path_failure_rate *= computer_failure_rate(platform, computer_name)
        if path_failure_rate == function_failure_rate:
            count_equal += 1
        if path_failure_rate > function_failure_rate:
            function_failure_rate = path_failure_rate
            count_equal = 1
    return function_failure_rate


def run(
    allocation_algorithm: AllocationAlgorithm,
    schedulability_analysis: SchedulabilityAnalysis,
    platform: Platform,
    app_list: list[Application],
):

    # Create list of functions
    function_list = {}
    for app in app_list:
        if app.system_function in function_list:
            function_list[app.system_function].append(app)
        else:
            function_list[app.system_function] = [app]

    initial_state = SystemState(platform, "S")

    try:
        # Perform initial allocation
        not_allocated_applications = allocation_algorithm.run(
            copy.copy(app_list), platform
        )
        if len(not_allocated_applications) > 0:
            initial_state.set_status(SchedulabilityStatus.SCHEDULABLE_DEGRADED)
        else:
            initial_state.set_status(SchedulabilityStatus.SCHEDULABLE)

        logging.debug(platform)

        schedulability_status = schedulability_analysis.is_schedulable(
            platform, f"{allocation_algorithm.name}-S"
        )
    except mexceptions.AllocationError as e:
        schedulability_status = SchedulabilityStatus.NOT_SCHEDULABLE
        not_allocated_applications = app_list
        logging.error(f"Error during initial allocation: {e}")

    schedulability_status = evaluate_system_status(
        schedulability_status, not_allocated_applications
    )
    initial_state.set_status(schedulability_status)

    logging.debug(f"State {initial_state.name}|{initial_state.status}")

    if initial_state.status != SchedulabilityStatus.NOT_SCHEDULABLE:
        # Analyse for all failure modes
        for i in range(len(platform.computers)):
            evaluate_computer_fault(
                i, initial_state, allocation_algorithm, schedulability_analysis
            )

    failure_paths, function_failure_rates = functions_failure_rate(
        function_list, initial_state
    )

    logging.debug(failure_paths)
    logging.debug(function_failure_rates)

    return initial_state, failure_paths, function_failure_rates


def functions_failure_rate(function_list, root: SystemState):
    failure_paths = {}
    for key, value in function_list.items():
        paths = search_failure_paths(root, key, value)
        failure_paths[key] = paths

    function_failure_rates = {}
    for function_name, paths in failure_paths.items():
        function_failure_rates[function_name] = calculate_function_failure_rate(
            root.platform, paths
        )
    return failure_paths, function_failure_rates


def num_of_states(root: SystemState, status: SchedulabilityStatus):
    if root.status == status:
        count = 1
    else:
        count = 0

    for state in root.next_states:
        count += num_of_states(state, status)

    return count


def export_state(root: SystemState, root_path):
    current_path = f"{root_path}/{root.name}"
    try:
        os.mkdir(current_path)
    except FileExistsError:
        pass
    with open(f"{current_path}/state_info.csv", "w") as file:
        # Writing data to a file
        file.write(str(root))

    for state in root.next_states:
        export_state(state, root_path)


def get_states_info(root: SystemState, data=None) -> dict:
    if data is None:
        data = {}

    data[root.name] = {}
    data[root.name]["State"] = root.status.name
    data[root.name]["Balance Ratio"] = root.platform.balance_ratio()
    load_sum = 0
    partition_count = 0
    for computer in root.platform.computers:
        for partition in computer.partitions:
            load_sum += partition.load()
            partition_count += 1

    data[root.name]["Load Average"] = load_sum / partition_count
    data[root.name]["Partition Count"] = partition_count
    for state in root.next_states:
        get_states_info(state, data)

    return data


def export_results_csv(
    root: SystemState,
    function_failure_rates,
    input_name,
    algorithm_name,
    file_path,
):
    with open(file_path, "a+", encoding="utf-8") as file:
        entry = ""
        entry += f"{algorithm_name},{input_name},"
        for key, value in function_failure_rates.items():
            entry += f"{value},"

        states_number = {
            "SCHEDULABLE": 0,
            "SCHEDULABLE_DEGRADED": 0,
            "NOT_SCHEDULABLE": 0,
        }
        states_number["SCHEDULABLE"] = num_of_states(
            root, SchedulabilityStatus.SCHEDULABLE
        )
        states_number["SCHEDULABLE_DEGRADED"] = num_of_states(
            root, SchedulabilityStatus.SCHEDULABLE_DEGRADED
        )
        states_number["NOT_SCHEDULABLE"] = num_of_states(
            root, SchedulabilityStatus.NOT_SCHEDULABLE
        )

        entry += f'{states_number["SCHEDULABLE"]},{states_number["SCHEDULABLE_DEGRADED"]},{states_number["NOT_SCHEDULABLE"]},'

        states_info = get_states_info(root)
        balance_ratio_sum = {
            "SCHEDULABLE": 0,
            "SCHEDULABLE_DEGRADED": 0,
            "NOT_SCHEDULABLE": 0,
        }
        load_sum = {
            "SCHEDULABLE": 0,
            "SCHEDULABLE_DEGRADED": 0,
            "NOT_SCHEDULABLE": 0,
        }
        load_count = {
            "SCHEDULABLE": 0,
            "SCHEDULABLE_DEGRADED": 0,
            "NOT_SCHEDULABLE": 0,
        }
        for state_name, state_info in states_info.items():
            balance_ratio_sum[state_info["State"]] += state_info["Balance Ratio"]
            load_sum[state_info["State"]] += state_info["Load Average"]

        for key, value in balance_ratio_sum.items():
            if states_number[key] > 0:
                entry += f"{value/states_number[key]},"
            else:
                entry += ","

        for key, value in load_sum.items():
            if states_number[key] > 0:
                entry += f"{value/states_number[key]},"
            else:
                entry += ","

        entry += "\n"
        file.write(entry)


def export_results(root: SystemState, failure_paths, function_failure_rates, path):

    reconfiguration_graph = SystemReconfigurationGraph(root)

    try:
        os.mkdir(path)
        os.mkdir(f"{path}/images")
    except FileExistsError:
        pass

    # save graph image
    file_name = f'{path}/images/{datetime.now().strftime("%d%m%Y_%H-%M-%S")}.png'
    reconfiguration_graph.graph(file_name)

    data = {}
    data["Functions Failure Rate"] = function_failure_rates
    data["Full Schedulable States"] = num_of_states(
        root, SchedulabilityStatus.SCHEDULABLE
    )
    data["Degraded Schedulable States"] = num_of_states(
        root, SchedulabilityStatus.SCHEDULABLE_DEGRADED
    )
    data["Not Schedulable States"] = num_of_states(
        root, SchedulabilityStatus.NOT_SCHEDULABLE
    )

    data["Failure Paths"] = failure_paths

    data["states"] = get_states_info(root)

    with open(f"{path}/result_summary.json", "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    # Writing to file

    try:
        os.mkdir(f"{path}/states")
    except FileExistsError:
        pass

    export_state(root, f"{path}/states")
