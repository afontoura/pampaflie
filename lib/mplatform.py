import copy
import csv
import logging
import os
import sys
import xml.etree.cElementTree as ET
from abc import ABCMeta
from dataclasses import dataclass
from enum import IntEnum
from statistics import mean, stdev
from textwrap import fill
from typing import List

import lib.mexceptions as mexceptions


class ApplicationDAL(IntEnum):
    NA = 0
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5


@dataclass
class ApplicationAddress:
    partition: int = 0
    computer: int = 0
    application: int = 0


@dataclass
class Application:
    """Contains an application data
    Implements sorting method
    """

    name: str
    period: float
    wcet: float
    dal: ApplicationDAL
    independent: str = None
    system_function: str = None
    time: float = 0.0
    ahp_rating: float = None
    balance_try: bool = False
    address: ApplicationAddress = None

    def __post_init__(self) -> None:
        """It is called after init and it calculated the worst case time over 1ms"""
        self.time = float(self.wcet) / float(self.period)

    def __lt__(self, other):
        """Method for the sort call. It sorts the data according to following priorities:
        - for min-min algorithm: DAL and then Period
        - for ahp algorithm: ahp rating

        TO-DO:  move this business logic to the algorithm class

        Args:
            other (Application): Application to be compared with

        Returns:
            boolean: if current object is less than other
        """
        if self.ahp_rating is None:
            if self.dal == other.dal:
                return self.time < other.time
            else:
                return self.dal < other.dal
        else:
            return self.ahp_rating < other.ahp_rating

    def __str__(self) -> str:
        return f"{self.name}: {self.period} | {self.wcet} | {self.dal} | {self.time}"


class Partition:
    DEFAULT_PREEMPTION_JITTER: float = 0.01
    applications: List[Application]
    size: float
    remaining_time: float
    preemption_jitter: float = DEFAULT_PREEMPTION_JITTER
    partition_dal: ApplicationDAL = ApplicationDAL.NA
    computer_major_frame: float = 0.0
    address: ApplicationAddress = None

    def __init__(
        self,
        size=0.0,
        address: ApplicationAddress = None,
        major_frame: float = 0.0,
        applications=None,
    ):
        self.size = float(size)
        self.applications = [] if applications is None else applications
        self.remaining_time = self.size
        self.computer_major_frame = major_frame
        if address is not None:
            self.address = address

    def load(self) -> float:
        return self.remaining_time / self.size

    def contains_application(self, application_name: str) -> bool:
        for app in self.applications:
            if app.name == application_name:
                return True

        return False

    def check_constraints(self, application):
        if not self._dal_constraint(application.dal):
            raise mexceptions.PartitionCritical(
                f"Partition ({self.partition_dal.name}) too critical for {application.name}({application.dal.name})"
            )

    def _dal_constraint(self, new_dal: ApplicationDAL):
        if self.partition_dal == ApplicationDAL.NA:
            return True
        if new_dal == self.partition_dal:
            return True
        # elif (new_dal==ApplicationDAL.B and self.partition_dal==ApplicationDAL.A) or (new_dal==ApplicationDAL.A and self.partition_dal==ApplicationDAL.B):
        #     return True
        else:
            return False

    def _time_cost(self, application: Application) -> float:
        """Calculate the time an application requires to run in a partition
        given the computer major frame where the application is located and the application
        characteristics.

        Args:
            application (Application): the application to be allocated into a partition

        Returns:
            float: time cost
        """
        return application.wcet * self.computer_major_frame / application.period

    def pop_application(self, index: int = -1) -> Application:
        """Remove application from the partition
        It recalculates the remaining time available in the partition and adjust new application addresses


        Args:
            index (int, optional): index of the application within the partition. Defaults to -1.

        Returns:
            Application: removed application
        """
        application = self.applications.pop(index)
        self.remaining_time += self.preemption_jitter + self._time_cost(application)

        if len(self.applications) > 0:
            new_dal = ApplicationDAL.D
            # rearrange application addresses
            for i in range(len(self.applications)):
                self.applications[i].address.application = i + 1
                if self.applications[i].dal < new_dal:
                    new_dal = self.applications[i].dal
            self.partition_dal = new_dal
        else:
            self.partition_dal = ApplicationDAL.NA

        return application

    def append_application(self, application: Application):
        self.check_constraints(application)

        new_remaining_time = (
            self.remaining_time - self.preemption_jitter - self._time_cost(application)
        )
        if new_remaining_time < 0.0:
            raise mexceptions.PartitionFullError(
                f"Full when trying to allocation App {application.name}. Remaining Time: {new_remaining_time}"
            )
        else:
            self.remaining_time = new_remaining_time
            self.applications.append(application)
            application.address = ApplicationAddress(
                self.address.partition, self.address.computer, len(self.applications)
            )
            if (
                self.partition_dal > application.dal
                or self.partition_dal == ApplicationDAL.NA
            ):
                self.partition_dal = application.dal

    def __str__(self) -> str:
        return str(self.size)


class Computer:
    partitions: List[Partition]
    major_frame: float
    computer_address: int
    name: str

    def __init__(
        self,
        computer_item,
        computer_address: int = 0,
        major_frame: float = -1.0,
        failure_rate: float = 0.0,
    ):
        self.computer_address = computer_address
        self.partitions = []
        self.major_frame = major_frame
        self.name = f"C{self.computer_address}"
        self.failure_rate = failure_rate
        logging.debug(computer_item)
        for partition_size in computer_item:
            try:
                self.partitions.append(
                    Partition(
                        size=partition_size,
                        address=ApplicationAddress(
                            len(self.partitions) + 1, computer_address
                        ),
                        major_frame=major_frame,
                    )
                )
            except ValueError as e:
                logging.debug("Partition Skipped")
                continue

    def contains_application(self, application_name: str) -> bool:
        for partition in self.partitions:
            if partition.contains_application(application_name):
                return True
        return False

    def check_constraints(self, application):
        if self.contains_application(application.independent):
            raise mexceptions.ApplicationDependendConstraint(
                f"{application.name} cannot coexist with {application.independent}"
            )

    def append_application(self, application: Application):
        # TODO: check if application period and wcet is compatible with computer major frame

        self.check_constraints(application)
        partition_index = 0
        for partition in self.partitions:
            partition_index += 1
            try:
                partition.append_application(application)
                logging.debug(f"{partition_index}")
                return partition_index
            except (mexceptions.PartitionFullError, mexceptions.PartitionCritical) as e:
                logging.debug(f"{partition_index}: {e}")
                continue

        raise mexceptions.ComputerNotSuitableError(
            f"{application.name} not suitable for this computer"
        )

    def __str__(self) -> str:
        return_str = ""
        for partition in self.partitions:
            return_str += f" {self.major_frame} : {partition}  | "
        return return_str


class Platform:
    computers: List[Computer]

    def __init__(self, platform_csv_file_name: str = ""):
        computer_list = self._read_platform_csv(platform_csv_file_name)
        self._computer_list_to_computers(computer_list)

    def _computer_list_to_computers(self, computer_list: list):
        self.computers = []
        for computer_item in computer_list:
            self.computers.append(
                Computer(
                    computer_item=computer_item["Partitions"],
                    major_frame=computer_item["MF"],
                    computer_address=len(self.computers) + 1,
                    failure_rate=computer_item["Failure Rate"],
                )
            )

    def _read_platform_csv(self, filename: str) -> list:
        csv_platform = open(filename, newline="", encoding="utf-8-sig")
        platform_data = csv.reader(csv_platform, delimiter=",")
        computer_list = []
        next(platform_data, None)
        for row in platform_data:
            # if row[0] == 'Computer':
            #     continue

            partitions = []
            for i in range(3, len(row)):
                partitions.append(row[i])

            computer_list.append(
                {
                    "MF": float(row[2]),
                    "Failure Rate": float(row[1]),
                    "Partitions": partitions,
                }
            )

        return computer_list

    def balance_ratio(self) -> float:
        """The balance ratio of a platform is defined by the coefficient of covariance
        of load ratio of partition

        Args:
            platform (Platform): Platform in which a balance ration should be calculated

        returns:
        """
        partitions_availability_ratio = []
        for computer in self.computers:
            for partition in computer.partitions:
                partitions_availability_ratio.append(
                    partition.remaining_time / partition.size
                )

        return stdev(partitions_availability_ratio) / mean(
            partitions_availability_ratio
        )

    def pop_application_at_address(self, address: ApplicationAddress) -> Application:
        try:
            # if address.application is not provided (0 by default as per class ApplicationAddress), the last item in the array will be poped (-1)
            application = (
                self.computers[address.computer - 1]
                .partitions[address.partition - 1]
                .pop_application(address.application - 1)
            )
            return application
        except IndexError as e:
            raise mexceptions.PartitionEmpty(
                f"Error while trying to remove application. C{address.computer}P{address.partition} is Empty"
            )

    def append_application_at_address(
        self, address: ApplicationAddress, application: Application
    ):
        self.computers[address.computer - 1].check_constraints(application)
        self.computers[address.computer - 1].partitions[
            address.partition - 1
        ].append_application(application)

    def _update_addresses(self):
        computer_address = 1
        for computer in self.computers:
            partition_index = 1
            computer.computer_address = computer_address
            for partition in computer.partitions:
                partition.address.computer = computer_address
                partition.address.partition = partition_index
                for application in partition.applications:
                    application.address.computer = computer_address
                    application.address.partition = partition_index
                partition_index += 1
            computer_address += 1

    def remove_computer(self, computer_index) -> list[Application]:
        removed_applications = []
        computer = self.computers.pop(computer_index)
        for partition in computer.partitions:
            removed_applications.extend(partition.applications)

        self._update_addresses()

        return removed_applications

    def csv_export_str(self) -> str:
        """Export the platform as csv structured string

        Returns:
            str: csv formatted platform
        """
        export_computers = []
        for computer in self.computers:
            export_partitions = []
            for partition in computer.partitions:
                export_applications = []
                for application in partition.applications:
                    export_applications.append(application.name)
                export_partitions.append(export_applications)
            export_computers.append(export_partitions)

        export_str = ""
        computer_index = 1
        for computer in self.computers:
            for partition in computer.partitions:
                export_str += f",{computer.name}"
            computer_index += 1

        export_str += "\n"
        for computer in self.computers:
            partition_index = 1
            for partition in computer.partitions:
                export_str += f",{partition_index}"
                partition_index += 1

        export_str += "\n"
        for computer in self.computers:
            for partition in computer.partitions:
                export_str += f",{partition.size}"

        max_counter = 0
        for computer in export_computers:
            for partition in computer:
                if len(partition) > max_counter:
                    max_counter = len(partition)

        index = 0
        while index < max_counter:
            export_str += "\n"
            for computer in export_computers:
                for partition in computer:
                    if len(partition) > index:
                        export_str += f",{partition[index]}"
                    else:
                        export_str += ","

            index += 1

        export_str += "\n"
        for computer in self.computers:
            for partition in computer.partitions:
                export_str += f",{partition.remaining_time}"

        export_str += "\n"
        for computer in self.computers:
            for partition in computer.partitions:
                export_str += f",{partition.load()}"

        return export_str

    def __str__(self) -> str:
        return str(self.csv_export_str())
        return_str = "Name      : MF : Partition...\n"
        for i in range(len(self.computers)):
            return_str += f"Computer {i}: {self.computers[i]}\n"
        return return_str


class PlatformExport:
    def __init__(self, platform: Platform, file_location_path: str = ""):
        self._platform = platform
        self._file_location_path = file_location_path

    def export(self):
        raise NotImplementedError
