import copy
import csv
import getopt
import logging
import sys

import lib.framework as framework
from lib.malgorithm import (
    AlgorithmAHP,
    AlgorithmAHPPostBalance,
    AlgorithmAHPResourceAware,
    AlgorithmAHPRoundRobin,
    AlgorithmGreedy,
    AlgorithmMinMin,
    AlgorithmMinMinNoBalance,
    AlgorithmType,
    AllocationAlgorithm,
)
from lib.manalysis import (
    CheddarAnalysis,
    NotASchedulabilityAnalysis,
    SchedulabilityType,
)
from lib.mplatform import Application, ApplicationDAL, Platform


def read_application(filename: str) -> list[Application]:
    csv_application = open(filename, newline="", encoding="utf-8-sig")
    application_data = csv.DictReader(csv_application)
    app_list = []
    for row in application_data:
        new_application = Application(
            name=row["Name"],
            period=float(row["Period"]),
            wcet=float(row["WCET"]),
            dal=ApplicationDAL(int(row["DAL"])),
            independent=row["Independent"],
            system_function=row["Function"],
        )
        app_list.append(new_application)

    return app_list


def configure_logging(log_level):
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[logging.StreamHandler(), logging.FileHandler("output.log", mode="w")],
    )


def choose_allocation_algorithm(
    user_input: str, additional_input=""
) -> AllocationAlgorithm:
    # Choose allocation algorithm

    logging.debug(user_input)

    chose_algorithm = user_input.upper()
    if chose_algorithm == AlgorithmType.GREEDY.name:
        logging.info("Running Greedy Algorithm")
        return AlgorithmGreedy()
    elif chose_algorithm == AlgorithmType.MINMIN.name:
        logging.info("Running MINMIN Algorithm")
        return AlgorithmMinMin()
    elif chose_algorithm == AlgorithmType.AHP.name:
        logging.info("Running AHP Algorithm")
        return AlgorithmAHP(additional_input)
    elif chose_algorithm == AlgorithmType.AHP_ROUNDROBIN.name:
        logging.info("Running AHP Round Robin Algorithm")
        return AlgorithmAHPRoundRobin(additional_input)
    elif chose_algorithm == AlgorithmType.AHP_RESOURCE_AWARE.name:
        logging.info("Running AHP Resource Aware Algorithm")
        return AlgorithmAHPResourceAware(additional_input)
    elif chose_algorithm == AlgorithmType.AHP_POST_BALANCE.name:
        logging.info("Running AHP Post Balance Algorithm")
        return AlgorithmAHPPostBalance(additional_input)
    elif chose_algorithm == AlgorithmType.MINMIN_NO_BALANCE.name:
        logging.info("Running MINMIN No Balance")
        return AlgorithmMinMinNoBalance()

    else:
        return AlgorithmMinMin()


def choose_schedulability_analysis_tool(user_input: str):
    # Choose Schedulability Analysis Tool

    logging.debug(user_input)

    chosen_schedulability_analysis = user_input.upper()
    if chosen_schedulability_analysis == SchedulabilityType.CHEDDAR.name:
        return CheddarAnalysis()
    elif (
        chosen_schedulability_analysis
        == SchedulabilityType.NOT_A_SCHEDULABILITY_ANALYSIS.name
    ):
        return NotASchedulabilityAnalysis()
    else:
        return NotASchedulabilityAnalysis()


def read_inputs(platform_file: str, applications_file: str):
    # Read inputs (applications, platform)
    platform = Platform(platform_csv_file_name=platform_file)
    app_list = read_application(applications_file)

    logging.debug(platform)
    logging.debug(app_list)

    return platform, app_list


def print_usage():
    print("python pampaflie.py [options]")
    print("options:")
    print(
        "\t -a [name], --algorithm=[name]: chosen allocation algorithm (i.e. MINMIN, AHP, GREEDY)"
    )
    print(
        "\t -r [file_path], --ratingsfile=[file_path]: if AHP as chosen allocation algorithm, an additional input file is required"
    )
    print("\t\t supported formats: csv ")
    print("\t -p [file_path], --platform=[file_path]: platform input file")
    print("\t\t supported formats: csv, aadl (under construction) ")
    print("\t -t [file_path], --application=[file_path]: applications input file")
    print("\t\t supported formats [file_path]: csv, aadl (under construction) ")
    print(
        "\t -s [tool_name], --schedulability=[tool_name]: chosen schedulability snalysis tool (i.e. CHEDDAR"
    )
    print("\t -d : debug level for logging")


def handle_input(argv):
    # initialize user input defaults
    chose_algorithm = AlgorithmType.AHP.name
    platform_file = "inputs/input_platform.csv"
    application_file = "inputs/input_application.csv"
    ratings_file = "inputs/AHP_ratings.csv"
    log_level = logging.INFO
    chosen_schedulability_analysis = "asd"  # SchedulabilityType.CHEDDAR.name

    # parse user inputs
    try:
        opts, args = getopt.getopt(
            argv,
            "a:p:t:s:r:d",
            [
                "algorithm=",
                "platform=",
                "application=",
                "schedulability=",
                "ratingsfile=",
            ],
        )
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-a", "--algorithm"):
            chose_algorithm = arg
        elif opt in ("-p", "--platform"):
            platform_file = arg
        elif opt in ("-t", "--application"):
            application_file = arg
        elif opt in ("-r", "--ratingsfile"):
            ratings_file = arg
        elif opt in ("-s", "--schedulability"):
            chosen_schedulability_analysis = arg
        elif opt in ("-d"):
            log_level = logging.DEBUG

    configure_logging(log_level)

    return (
        chose_algorithm,
        chosen_schedulability_analysis,
        platform_file,
        application_file,
        ratings_file,
    )


def main(argv):

    (
        chose_algorithm,
        chosen_schedulability_analysis,
        platform_file,
        application_file,
        ratings_file,
    ) = handle_input(argv)

    allocation_algorithm = choose_allocation_algorithm(chose_algorithm, ratings_file)

    schedulability_analysis = choose_schedulability_analysis_tool(
        chosen_schedulability_analysis
    )

    platform, app_list = read_inputs(platform_file, application_file)

    initial_state, failure_paths, function_failure_rates = framework.run(
        allocation_algorithm, schedulability_analysis, platform, app_list
    )

    framework.export_results_csv(
        initial_state,
        function_failure_rates,
        platform_file,
        allocation_algorithm.name,
        "results.csv",
    )

    framework.export_results(
        initial_state,
        failure_paths,
        function_failure_rates,
        f"results/{allocation_algorithm.name}",
    )

    logging.info("Framework has finished")


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
