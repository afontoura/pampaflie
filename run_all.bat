@echo off

CALL :run_all_algo %1 input_platform.csv
CALL :run_all_algo %1 input_platform_C1.csv
CALL :run_all_algo %1 input_platform_C2.csv
CALL :run_all_algo %1 input_platform_C3.csv
CALL :run_all_algo %1 input_platform_C4.csv
CALL :run_all_algo %1 input_platform_new.csv

EXIT /B %ERRORLEVEL%


:run_all_algo
    mkdir results    
    python .\pampaflie.py -s %1 -a GREEDY -p inputs/%2
    python .\pampaflie.py -s %1 -a MINMIN -p inputs/%2
    python .\pampaflie.py -s %1 -a AHP -p inputs/%2
    python .\pampaflie.py -s %1 -a AHP_ROUNDROBIN -p inputs/%2
    python .\pampaflie.py -s %1 -a AHP_RESOURCE_AWARE -p inputs/%2
    python .\pampaflie.py -s %1 -a AHP_POST_BALANCE -p inputs/%2

    xcopy results "agregated_results/%2"  /E /H /C /R /Q /Y /I
    rd /s /q results 
    mkdir results
    EXIT /B 0

